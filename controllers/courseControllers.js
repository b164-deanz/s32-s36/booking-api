const Course = require("../models/Course")

//Create new courses
/*
Steps:
1. Create new Courses object
2. Save to database
3. error handling

*/



module.exports.addCourse = (reqBody) => {
	console.log(reqBody)

	let newCourse = new Course ({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	})

	//D
	return newCourse.save().then((course, error) =>{
		//Courses creation failed
		if (error) {
			return false
		}else{
			//Course Creation successful
			return true
		}
	})
}


// module.exports.isAdminAuth = (reqBody)=> {
// 	return Course.findOne({reqBody}).then(result => {
// 		if(result.isAdmin === true){
// 			return true
// 		}else{
// 			return false
// 		}
// 	})
// }

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

//retrieve specific Courses
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	})
}

module.exports.updateCourse = (CourseId, reqBody) => {
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(CourseId, updatedCourse).then((course, error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.archiveCourse = (CourseId, reqBody) => {
	let archiveData = {
		isActive: false
	}
	return Course.findByIdAndUpdate(CourseId, archiveData).then((course, error) => {
		if (error) {
			return error
		}else{
			return true
		}
	})
}