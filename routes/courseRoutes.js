const express = require("express")
const router = express.Router()
const CourseController = require("../controllers/courseControllers")
const auth = require("../auth");

// router.post("/", auth.verify(req, res) => {
// 	CourseController.addCourse(req.body).then(result => res.send(result))
// })
 
router.post("/", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		CourseController.addCourse(data).then(result => res.send(result))
	}else {
		res.send({ auth: "You're not an admin"})
	}

})


// router.post("/", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization)
// 	admin = userData.isAdmin

// 	if(admin){

// 	CourseController.addCourse(userData.isAdmin).then(result => res.send(result))}
// 	else{
// 		return false
// 	}
// })

//retrieving all courses
router.get("/all" , (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result))
})

router.get("/", (req, res) =>{
	CourseController.getAllActive().then(result => res.send(result))
})

//retrive specific courses using ID
router.get("/:courseId", (req, res) =>{
	console.log(req.params.courseId)
	CourseController.getCourse(req.params.courseId).then(result => res.send(result))
})


//Update a Course
router.put("/:courseId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})

router.put("/:courseId/archive", auth.verify, (req, res) => {
	const courseData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(courseData.isAdmin){
		CourseController.archiveCourse(req.params.courseId, req.body).then(result => res.send(result))
	}
	else{
		res.send(false)
	}
})







module.exports = router;